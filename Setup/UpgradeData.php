<?php

namespace ITP\edtCustomer\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements UpgradeDataInterface
{
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if ($context->getVersion()
            && version_compare($context->getVersion(), '0.0.2') < 0
        ) {
            $table = $setup->getTable('foo_table');
            $setup->getConnection()
                ->insertForce($table, ['videogame' => 'Halo 3: ODST', 'release' => 'Anywhere by here']);

            $setup->getConnection()
                ->update($table, ['videogame' => 'Halo Reach'], 'id IN (1,2)');
        }
        $setup->endSetup();
    }
}